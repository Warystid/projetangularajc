export interface Formateur {
    id:number;
    nom:string;
    prenom:string;
    type?:string;
}
