import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FindModuleComponent } from './find-module/find-module.component';
import { PostModuleComponent } from './post-module/post-module.component';
import { DeleteModuleComponent } from './delete-module/delete-module.component';
import { UpdateModuleComponent } from './update-module/update-module.component';
import { PageModuleComponent } from './page-module/page-module.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    FindModuleComponent,
    PostModuleComponent,
    DeleteModuleComponent,
    UpdateModuleComponent,
    PageModuleComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forRoot([
      { path: 'find-module', component: FindModuleComponent },
      { path: 'update-module', component: UpdateModuleComponent },
      { path: 'delete-module', component: DeleteModuleComponent },
      { path: 'post-module', component: PostModuleComponent },
      { path: 'page-module/:arg', component: PageModuleComponent }
    ])
  ],
  exports:[FindModuleComponent,PostModuleComponent,DeleteModuleComponent,UpdateModuleComponent,PageModuleComponent]
})
export class ModuleModule { }
