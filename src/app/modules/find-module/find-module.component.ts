import { Component, OnInit } from '@angular/core';
import { Module } from '../module';
import { ModuleService } from '../module.service';


@Component({
  selector: 'find-module',
  templateUrl: './find-module.component.html',
  styleUrls: ['./find-module.component.css']
})
export class FindModuleComponent implements OnInit {
  modules:Module[]=[];

  constructor(private serviceMod: ModuleService) { 
    const source$=this.serviceMod.getModules().subscribe((res:any)=>{
      this.modules=res;
    })
  }

  ngOnInit(): void {
  }

}
