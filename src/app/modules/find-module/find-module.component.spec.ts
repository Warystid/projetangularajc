import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FindModuleComponent } from './find-module.component';

describe('FindModuleComponent', () => {
  let component: FindModuleComponent;
  let fixture: ComponentFixture<FindModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FindModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FindModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
