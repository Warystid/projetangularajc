import { Component, OnInit } from '@angular/core';
import { Module } from '../module';
import { ModuleService } from '../module.service';

@Component({
  selector: 'post-module',
  templateUrl: './post-module.component.html',
  styleUrls: ['./post-module.component.css']
})
export class PostModuleComponent implements OnInit {
  module:Module={id:0,dateDebut:new Date(),dateFin:new Date(),libelle:"",filiere:0,formateur:{id:0,nom:"",prenom:""}}

  constructor(private serviceMod: ModuleService) { }

  handleSubmit() {
    this.serviceMod.postModule(this.module)
    .subscribe((res:any)=>{
      console.log("Ajouté");
    });
  }

  ngOnInit(): void {
  }

}
