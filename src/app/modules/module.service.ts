import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Module } from './module';
import { Observable } from 'rxjs';

const api = "http://localhost:8082/api/module";

@Injectable({
  providedIn: 'root'
})
export class ModuleService {

  constructor(private http:HttpClient) {
    
   }

   getModules(): Observable<Module[]>{
    return this.http.get<Module[]>(api);
  }

  getModuleById(module: Module): Observable<Module>{
    return this.http.get<Module>(api+"/"+module.id);
  }

  postModule(module:Module){
    return this.http.post<Module>(api,module);
  } 

  updateModule(module:Module){
    return this.http.put<Module>(api,module);
  } 

  deleteModule(module:Module){
    return this.http.delete<Module>(api+"/"+module.id);
  } 


}
