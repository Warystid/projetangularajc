import { Component, OnInit } from '@angular/core';
import { Module } from '../module';
import { ModuleService } from '../module.service';

@Component({
  selector: 'update-module',
  templateUrl: './update-module.component.html',
  styleUrls: ['./update-module.component.css']
})
export class UpdateModuleComponent implements OnInit {
  module:Module={id:0,dateDebut:new Date(),dateFin:new Date(),libelle:"",filiere:0,formateur:{id:0,nom:"",prenom:""}}

  constructor(private serviceMod: ModuleService) { }

  handleSubmit() {
    this.serviceMod.updateModule(this.module)
    .subscribe((res:any)=>{
      console.log("Modifié");
    });
  }

  ngOnInit(): void {
  }

}
