import { Component, OnInit } from '@angular/core';
import { Module } from '../module';
import { ModuleService } from '../module.service';

@Component({
  selector: 'delete-module',
  templateUrl: './delete-module.component.html',
  styleUrls: ['./delete-module.component.css']
})
export class DeleteModuleComponent implements OnInit {
  module:Module={id:0,dateDebut:new Date(),dateFin:new Date(),libelle:"",filiere:0,formateur:{id:0,nom:"",prenom:""}}

  constructor(private serviceMod: ModuleService) { }

  handleSubmit() {
    this.serviceMod.deleteModule(this.module)
    .subscribe((res:any)=>{
      console.log("Supprimé");
    });
  }

  ngOnInit(): void {
  }

}
