import { Formateur } from "./formateur";

export interface Module {
    id?:number;
    dateDebut:Date;
    dateFin:Date;
    libelle:string;
    filiere?:number;
    formateur?:Formateur;
}
