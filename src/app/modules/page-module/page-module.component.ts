import { formatNumber } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Formateur } from '../formateur';
import { Module } from '../module';
import { ModuleService } from '../module.service';

@Component({
  selector: 'page-module',
  templateUrl: './page-module.component.html',
  styleUrls: ['./page-module.component.css']
})
export class PageModuleComponent implements OnInit {
  urlParam: string | null = '';
  module:Module={id:0,dateDebut:new Date(),dateFin:new Date(),libelle:"",filiere:0,formateur:{id:0,nom:"",prenom:""}}
  formateur:Formateur={id:0,nom:"",prenom:""};

  constructor(private route: ActivatedRoute, private serviceMod: ModuleService) { }

  ngOnInit(): void {
    this.urlParam = this.route.snapshot.paramMap.get('arg');
     this.module.id=Number(this.urlParam);
     
     this.serviceMod.getModuleById(this.module)
    .subscribe((res:any)=>{
      this.module=res;
      this.formateur=res.formateur;
    });


  }

}
