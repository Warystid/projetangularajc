import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostFiliereComponent } from './post-filiere.component';

describe('PostFiliereComponent', () => {
  let component: PostFiliereComponent;
  let fixture: ComponentFixture<PostFiliereComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostFiliereComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostFiliereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
