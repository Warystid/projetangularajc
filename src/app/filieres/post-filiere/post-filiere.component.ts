import { Component, OnInit } from '@angular/core';
import { Filiere } from '../filiere';
import { FiliereService } from '../filiere.service';

@Component({
  selector: 'post-filiere',
  templateUrl: './post-filiere.component.html',
  styleUrls: ['./post-filiere.component.css']
})
export class PostFiliereComponent implements OnInit {
  filiere:Filiere={libelle:"",modules:[],stagiaires:[]};

  constructor(private serviceFil: FiliereService) { }

  handleSubmit() {
    this.serviceFil.postFiliere(this.filiere)
    .subscribe((res:any)=>{
      console.log("Ajouté");
    });
  }

  ngOnInit(): void {
  }

}
