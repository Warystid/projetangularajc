import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FiliereComponent } from './find-filiere/filiere.component';
import { PostFiliereComponent } from './post-filiere/post-filiere.component';
import { FormsModule } from '@angular/forms';
import { UpdateFiliereComponent } from './update-filiere/update-filiere.component';
import { DeleteFiliereComponent } from './delete-filiere/delete-filiere.component';
import { RouterModule } from '@angular/router';
import { PageFiliereComponent } from './page-filiere/page-filiere.component';

@NgModule({
  declarations: [FiliereComponent, PostFiliereComponent, UpdateFiliereComponent, DeleteFiliereComponent, PageFiliereComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forRoot([
      { path: 'find-filiere', component: FiliereComponent },
      { path: 'update-filiere', component: UpdateFiliereComponent },
      { path: 'delete-filiere', component: DeleteFiliereComponent },
      { path: 'post-filiere', component: PostFiliereComponent },
      { path: 'page-filiere/:arg', component: PageFiliereComponent }
    ])
  ],
  exports:[FiliereComponent, PostFiliereComponent, UpdateFiliereComponent, DeleteFiliereComponent]
})
export class FiliereModule { }
