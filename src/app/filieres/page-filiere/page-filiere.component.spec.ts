import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageFiliereComponent } from './page-filiere.component';

describe('PageFiliereComponent', () => {
  let component: PageFiliereComponent;
  let fixture: ComponentFixture<PageFiliereComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageFiliereComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageFiliereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
