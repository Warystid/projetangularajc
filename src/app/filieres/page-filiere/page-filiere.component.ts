import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Module } from 'src/app/modules/module';
import { Filiere } from '../filiere';
import { FiliereService } from '../filiere.service';

@Component({
  selector: 'page-filiere',
  templateUrl: './page-filiere.component.html',
  styleUrls: ['./page-filiere.component.css']
})
export class PageFiliereComponent implements OnInit {
  urlParam: string | null = '';
  filiere:Filiere={id:0,libelle:"",modules:[],stagiaires:[]};
  modules:Module[]=[];

  constructor(private route: ActivatedRoute, private serviceFil: FiliereService) { }

  ngOnInit(): void {
     this.urlParam = this.route.snapshot.paramMap.get('arg');
     this.filiere.id=Number(this.urlParam);
     
     this.serviceFil.getFilieresById(this.filiere)
    .subscribe((res:any)=>{
      this.filiere=res;
    });

    this.serviceFil.getModulesOfFiliere(this.filiere)
    .subscribe((res:any)=>{
      this.modules=res;
    });
  }

}
