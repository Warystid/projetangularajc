import { Component, OnInit } from '@angular/core';
import { Filiere } from '../filiere';
import { FiliereService } from '../filiere.service';

@Component({
  selector: 'filiere',
  templateUrl: './filiere.component.html',
  styleUrls: ['./filiere.component.css']
})
export class FiliereComponent implements OnInit {
  filieres: Filiere[]=[];

  constructor(private serviceFil: FiliereService) {
      const source$=this.serviceFil.getFilieres().subscribe((res:any)=>{
        this.filieres=res;
      })
   }


  ngOnInit(): void {
  }

}
