import { Component, OnInit } from '@angular/core';
import { Filiere } from '../filiere';
import { FiliereService } from '../filiere.service';

@Component({
  selector: 'update-filiere',
  templateUrl: './update-filiere.component.html',
  styleUrls: ['./update-filiere.component.css']
})
export class UpdateFiliereComponent implements OnInit {
  filiere:Filiere={id:0,libelle:"",modules:[],stagiaires:[]};

  constructor(private serviceFil: FiliereService) { }

  handleSubmit() {
    this.serviceFil.updateFiliere(this.filiere)
    .subscribe((res:any)=>{
      console.log("Modifié");
    });
  }

  ngOnInit(): void {
  }

}
