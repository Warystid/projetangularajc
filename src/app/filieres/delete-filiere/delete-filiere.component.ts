import { Component, OnInit } from '@angular/core';
import { Filiere } from '../filiere';
import { FiliereService } from '../filiere.service';

@Component({
  selector: 'delete-filiere',
  templateUrl: './delete-filiere.component.html',
  styleUrls: ['./delete-filiere.component.css']
})
export class DeleteFiliereComponent implements OnInit {
  filiere:Filiere={id:0,libelle:"",modules:[],stagiaires:[]};

  constructor(private serviceFil: FiliereService) { }

  handleSubmit() {
    this.serviceFil.deleteFiliere(this.filiere)
    .subscribe((res:any)=>{
      console.log("Supprimé");
    });
  }

  ngOnInit(): void {
  }

}
