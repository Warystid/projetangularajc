import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteFiliereComponent } from './delete-filiere.component';

describe('DeleteFiliereComponent', () => {
  let component: DeleteFiliereComponent;
  let fixture: ComponentFixture<DeleteFiliereComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteFiliereComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteFiliereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
