import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Filiere } from './filiere';
import { Module } from '../modules/module';

const api = "http://localhost:8082/api/filiere";

@Injectable({
  providedIn: 'root'
})
export class FiliereService {

  constructor(private http:HttpClient) { }

  getFilieres(): Observable<Filiere[]>{
    return this.http.get<Filiere[]>(api);
  }

  getFilieresById(filiere: Filiere): Observable<Filiere>{
    return this.http.get<Filiere>(api+"/"+filiere.id);
  }

  postFiliere(filiere:Filiere){
    return this.http.post<Filiere>(api,filiere);
  } 

  updateFiliere(filiere:Filiere){
    return this.http.put<Filiere>(api,filiere);
  } 

  deleteFiliere(filiere:Filiere){
    return this.http.delete<Filiere>(api+"/"+filiere.id);
  } 

  getModulesOfFiliere(filiere: Filiere): Observable<Module[]>{
    return this.http.get<Module[]>("http://localhost:8082/api/module/filiere"+"/"+filiere.id);
  }


}
