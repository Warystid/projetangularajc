import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FiliereModule } from './filieres/filiere.module';
import { ModuleModule } from './modules/module.module';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FiliereModule,
    ModuleModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
